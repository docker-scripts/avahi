APP=avahi

### Avahi needs to have direct access on the LAN, in order to be able
### to receive and answer mDNS broadcat requests.  For this reason we
### are connecting it to an 'ipvlan' L2 bridge (which works with WiFi
### interfaces as well).
### For more details see:
### https://docs.docker.com/network/macvlan/#use-an-ipvlan-instead-of-macvlan
NETWORK_NAME=ipvlan-l2
NETWORK_SUBNET=192.168.1.0/24
NETWORK_GATEWAY=192.168.1.1
NETWORK_INTERFACE=wlp2s0b1
