# Avahi container

This container has `avahi-daemon`, which implements
[mDNS](https://en.wikipedia.org/wiki/Multicast_DNS). It can be used in
a LAN, along with **revproxy**, in order to support multiple local
domains.

By default `avahi-daemon` supports only one local domain per IP, which
is usually called `<hostname>.local`. If we want to have multiple
local domains (one for each container that is managed by **revproxy**),
we can support these domains with commands like this:
```bash
avahi-publish -a -R nextcloud.local 192.168.1.100 &
avahi-publish -a -R moodle.local 192.168.1.100 &
avahi-publish -a -R jitsi.local 192.168.1.100 &
```

This container does just this. It reads the local domains from a file
called `hosts.local`, and makes sure (through a cron job) that the
commands above are running. The file `hosts.local` looks like this:
```
nextcloud.local 192.168.1.100
moodle.local 192.168.1.100
jitsi.local 192.168.1.100
```

The IP doesn't have to be the same for all the hosts.

## Installation

  - First install `ds`:
    https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull avahi`

  - Create a directory for the container: `ds init avahi @avahi`

  - Fix the settings: `cd /var/ds/avahi/ ; vim settings.sh`

  - Build image, create the container and configure it: `ds make`
  
## Usage

Modify the file `/var/ds/avahi/hosts.local` by adding or removing
lines, and the changes will be reflected automatically (in 1 minute or
so).

## Networking

Avahi needs to have direct access on the LAN, in order to be able to
receive and answer mDNS broadcat requests. For this reason we are
connecting it to an 'ipvlan' L2 bridge (which works with WiFi
interfaces as well). For more details see:
https://docs.docker.com/network/macvlan/#use-an-ipvlan-instead-of-macvlan

Make sure to fix the details of networking on `settings.sh` before you
build the container.

## Other commands

```
ds stop
ds start
ds shell
ds help
```
