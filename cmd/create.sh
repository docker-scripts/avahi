cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    unset NETWORK    # we are using a custom network below
    _create_network
    orig_cmd_create \
        --network $NETWORK_NAME \
        "$@"    # accept additional options
    touch hosts.local
}

_create_network() {
    local network=$(docker network list --filter driver=ipvlan --format "{{.Name}}" | grep $NETWORK_NAME)
    [[ -n $network ]] && return
    docker network create -d ipvlan -o ipvlan_mode=l2 \
           --subnet=$NETWORK_SUBNET \
           --gateway=$NETWORK_GATEWAY \
           -o parent=$NETWORK_INTERFACE \
           $NETWORK_NAME
}
