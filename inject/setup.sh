#!/bin/bash -x

# create a script to update the processes
cat <<'EOF' > /usr/local/bin/avahi-publish.sh
#!/bin/bash

# kill any running processes
pkill -f 'avahi-publish -a -R '

# start new ones
while read line; do
    echo $line
    avahi-publish -a -R $line &
done < /host/hosts.local
EOF

# make it executable
chmod +x /usr/local/bin/avahi-publish.sh

# create a cron job to run this script every minute
cat <<EOF > /etc/cron.d/avahi-publish
* * * * *  root  /usr/local/bin/avahi-publish.sh
EOF
